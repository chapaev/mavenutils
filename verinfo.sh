#!/bin/sh

function extractver {
    echo $(sed 's/^[^>]*>\([^<]*\).*/\1/g' <<< $1)
}
extractver '<version>123</version>'
echo 'trunk/branch/tag'
grep -i '<version>' ./trunk/pom.xml | head -n 1 | sed 's/\s//g' | sed 's/^[^>]*>\([^<]*\).*/\1/g'
svn ls branches/ | sort -V | tail -n 1 | sed 's/\///g' | xargs -I{} grep -i '<version>' './branches/{}/pom.xml' | head -n 1 | sed 's/\s//g' | sed 's/^[^>]*>\([^<]*\).*/\1/g'
svn ls tags/ | sort -V | tail -n 1 | sed 's/\///g' | xargs -I{} grep -i '<version' './tags/{}/pom.xml' | head -n 1 | sed 's/\s//g' | sed 's/^[^>]*>\([^<]*\).*/\1/g'

TODO: сделать отдельный файл для работы с указанным POM-файлом