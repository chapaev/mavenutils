#!/bin/sh
if [ $# -eq 0 ]; then 
cat << EOF
This is private script. Don't use it!

Report bugs to: chapaev
EOF
exit 0
fi
###############################################################################
TRUE=1
FALSE=0
TAG_PATTERN='s/^[^>]*>\([^<]*\).*/\1/g'

function _exclude_tag 
{
    if [[ $line == *\<$1\>* ]]; then
        while [[ $line != *\<\/$1\>* ]]; do
            read line
        done
    fi
}

function _search_tag 
{
    while [[ $line != *\<$1\>* ]]; do
        read line
    done
}

function get_ver 
{
    artifactid=""
    version=""
    while read line; do
        _exclude_tag dependencies
        _exclude_tag build
        _exclude_tag parent
        if [[ $line == *\<version\>* ]]; then
            version=$line
            version=$(sed $TAG_PATTERN <<< $version)
            echo $version 
        fi
    done <$1
}

function get_snap_deps 
{
    while read line; do
        _search_tag dependencies
        while [[ $line != *\<\/dependencies\>* ]]; do
            read line
            if [[ $line == *\<dependency\>* ]]
            then
                artifactid=""
                version=""
                while [[ $line != *\<\/dependency\>* ]]; do
                    read line
                    if [[ $line == *artifactId* ]]; then
                        artifactid=$line
                    elif [[ $line == *version* ]]; then
                        version=$line
                    fi
                done
                if [[ $version == *SNAPSHOT* ]]; then
                    version=$(sed $TAG_PATTERN <<< $version)
                    artifactid=$(sed $TAG_PATTERN <<< $artifactid)
                    echo $artifactid $version $1 |  awk '{ printf "%-25s %-20s %s\n", $1, $2, $3}'
                fi                    
            fi
        done
        break
    done <$1
}


function get_art_use
{
    while read line; do
        _search_tag dependencies
        while [[ $line != *\<\/dependencies\>* ]]; do
            read line
            if [[ $line == *\<dependency\>* ]]
            then
                artifactid=""
                version=""
                while [[ $line != *\<\/dependency\>* ]]; do
                    read line
                    if [[ $line == *artifactId* ]]; then
                        artifactid=$line
                    elif [[ $line == *version* ]]; then
                        version=$line
                    fi
                done
                if [[ $artifactid == *$2* ]]; then
                    version=$(sed $TAG_PATTERN <<< $version)
                    echo $version $1 |  awk '{ printf "%-20s %s\n", $1, $2}'
                    break
                fi                    
            fi
        done
        break
    done <$1
}

function upd_art_ver
{
    readarray a < $1
    len=${#a[@]} 
    
    i=0    
    for ((; i<=$len; i++ ))
    do
        if [[ ${a[$i]} == *\<dependencies\>* ]]; then
            break
        fi        
    done
    
    for ((; i<=$len; i++ ))
    do
        if [[ ${a[$i]} == *$2* ]]; then
            break
        fi
    done

    isfinded=0
    for ((; i<=$len; i++ ))
    do
        if [[ ${a[$i]} == *\<\/dependencies\>* ]]; then
            break
        fi
        if [[ ${a[$i]} == *$2* ]]; then
            isfinded=1
            break
        fi
    done
    
    if [[ $isfinded == 1 ]]; then
        version=""
        while [[ ${a[$i]} != *\<\/dependency\>* ]]; do
            i=$(($i + 1))
            if [[ ${a[$i]} == *\<version\>* ]]; then
                version=${a[$i]}
                break
            fi
        done
        
        if [[ -z $version ]]; then
            while [[ ${a[$i]} != *\<dependency\>* ]]; do
                i=$(($i - 1))
                if [[ ${a[$i]} == *\<version\>* ]]; then
                    version=${a[$i]}
                    break
                fi
            done
            if [[ -z $version ]]; then
                echo Bad dependency version!
                exit
            fi
        fi
    fi
    
    i=$(($i + 1))
    sed -i $i's/^\([^>]*>\)[^<]*\(.*\)/\1'$3'\2/g' $1
}




if [[ $1 == '-get_ver' ]]; then
    get_ver $2
elif [[ $1 == '-get_snap_deps' ]]; then
    get_snap_deps $2
elif [[ $1 == '-get_art_use' ]]; then
    get_art_use $2 $3 
elif [[ $1 == '-upd_art_ver' ]]; then
    upd_art_ver $2 $3 $4
fi
