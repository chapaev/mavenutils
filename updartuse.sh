#!/bin/bash 
if [ $# -ne 2 ] || ([ $# -eq 1 ] && [ $1 == '--help' ]); then 
cat << EOF
This script updates version of dependency in all your projects.
Sctipr works only in trunk local folders.

usage: updartuse.sh artname version |[--help]

EXAMPLE
pc@user$ updartuse.sh mydependency 1.1

pc@user$ getartuse.sh mydependency
1.1                  ./project1/mvn/trunk/pom.xml
1.1                  ./project2/mvn/trunk/pom.xml

pc@user$ updartuse.sh mydependency 1.2

pc@user$ getartuse.sh mydependency
1.2                  ./project1/mvn/trunk/pom.xml
1.2                  ./project2/mvn/trunk/pom.xml

Report bugs to: chapaev
EOF
exit 0
fi
###############################################################################
for file in $(find -name pom.xml -path '*trunk*' -not -path '*/'$1'/*'); do
    pomutils.sh -upd_art_ver $file $1 $2
done
