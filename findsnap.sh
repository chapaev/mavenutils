#!/bin/bash 
if [ $# -eq 1 ] && [ $1 == '--help' ]; then 
cat << EOF
This script finds unstable dependencies that you use.
Script works only in trunk local folders.

usage: findsnap.sh [--help]

EXAMPLE
pc@user$ findsnap.sh 
dependency1               0.1.26-SNAPSHOT      ./project1/trunk/pom.xml
dependency2               0.1.13-SNAPSHOT      ./project2/trunk/pom.xml

Report bugs to: chapaev
EOF
exit 0
fi
###############################################################################
for file in $(find -name pom.xml -path '*trunk*'); do
    pomutils.sh -get_snap_deps $file
done    
