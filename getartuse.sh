#!/bin/bash 
if [ $# -ne 1 ] || ([ $# -eq 1 ] && [ $1 == '--help' ]); then 
cat << EOF
This script finds all projects that use your artifact by name.
Sctipr works only in trunk local folders.

usage: getartuse.sh mydependency | [--help]

EXAMPLE
pc@user$ getartuse.sh mydependency
1.1                  ./project1/mvn/trunk/pom.xml
0.2-SNAPSHOT         ./project2/mvn/trunk/pom.xml

Report bugs to: chapaev
EOF
exit 0
fi
###############################################################################
for file in $(find -name pom.xml -path '*trunk*' -not -path '*/'$1'/*' ); do
    pomutils.sh -get_art_use $file $1
done


