#!/bin/bash 
if [ $# -ne 1 ] || ([ $# -eq 1 ] && [ $1 == '--help' ]); then 
cat << EOF
This script finds actual version of your project by artifact name.
Script works only in trunk local folders.

usage: getartver.sh name | [--help]

EXAMPLE
pc@user$ getartver.sh myproj
0.1.26-SNAPSHOT  

Report bugs to: chapaev
EOF
exit 0
fi
###############################################################################
for file in $(find -name pom.xml -path '*'$1'*trunk*'); do
    pomutils.sh -get_ver $file
done 

