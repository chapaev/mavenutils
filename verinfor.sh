#!/bin/sh
svnurl=$(svn info | grep --binary-files="text" '^URL' | sed 's/URL:\s//')
echo 'last tag info'
svn ls tags/ | sort -V | tail -n 1 | sed 's/\///g' | xargs -I{} svn cat $svnurl'/tags/{}/pom.xml' | grep -i '<version>' | head -n 1 | sed 's/\s//g'
echo 'last branch info'
svn ls branches/ | sort -V | tail -n 1 | sed 's/\///g' | xargs -I{} svn cat $svnurl'/branches/{}/pom.xml' | grep -i '<version>' | head -n 1 | sed 's/\s//g'
echo 'trunk info'
svn cat $svnurl/trunk/pom.xml |grep -i '<version>' | head -n 1 | sed 's/\s//g'



